package com.example.lecture15

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.core.content.withStyledAttributes
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.card_view.view.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    var model:MyModel= MyModel()
    var admodel:MyModel.ad=MyModel.ad()
    var datalist= mutableListOf<MyModel.data>()
    var adlist= mutableListOf<MyModel.ad>()

    lateinit var adapter:Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


       GetUsers()
        refresh()

    }
    fun refresh(){
        refresh.setOnRefreshListener {
            refresh.isRefreshing=true
            datalist.clear()
            adlist.clear()
            adapter.notifyDataSetChanged()

            GetUsers()
            refresh.isRefreshing=false

        }
    }
    fun GetUsers(){
        RequestApi.getRequest("users",object :getJson{
            override fun onResponse(value: String) {
                d("hhh",value)
                setup(value)
            }

            override fun onFailure(value: String) {
               d("failure","failure")
            }

        })

    }

    private fun setup(jsonstring:String){




        val json=JSONObject(jsonstring)
        if(jsoncheck(json,"page")){
            model.page=json.getInt("page")

      }
        if(jsoncheck(json,"per_page")) {
        model.perPage=json.getInt("per_page")
            d("perpage",model.perPage.toString())
        }
        if(jsoncheck(json,"total")) {
            model.total=json.getInt("total")

        }
        if(jsoncheck(json,"total_pages")) {
            model.totalPages=json.getInt("total_pages")
        }
        if(jsoncheck(json,"data")) {
            var array= json.getJSONArray("data")


            (0 until array.length()).forEach(){
                i->
                var datajs=array.getJSONObject(i)
                datalist.add(MyModel.data(datajs.getInt("id"),datajs.getString("email"),datajs.getString("first_name"),datajs.getString("last_name"),datajs.getString("avatar")))

            }
        }

        if(jsoncheck(json,"ad")){
            var adjs=json.getJSONObject("ad")
            admodel.company=adjs.getString("company")
            admodel.text=adjs.getString("text")
            admodel.url=adjs.getString("url")
            adlist.add(admodel)
            model.adlist=adlist
            d("ddd",admodel.url)
        }

        Company.setText(admodel.company)
        Url.setText("url: "+admodel.url)
        text.setText(admodel.text.substring(0 ,admodel.text.length-30)+"...")

        adapter= Adapter(datalist)
        recycle.layoutManager=GridLayoutManager(this,2)
        recycle.adapter=adapter



        }



    fun jsoncheck(jsonobject:JSONObject,key:String):Boolean{

        if(!jsonobject.has(key)) return false
        return true

    }

}
