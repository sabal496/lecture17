package com.example.lecture15

import android.util.Log.d
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


object RequestApi {



    var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://reqres.in/api/")
        .build()


    var service = retrofit.create(Apiservice::class.java)

    fun getRequest(path :String,callback:getJson){
        val call=service.request(path)
        call.enqueue(object:  Callback<String>{
            override fun onFailure(call: retrofit2.Call<String>, t: Throwable) {
            callback.onFailure(t.message.toString())
            }

            override fun onResponse(call: retrofit2.Call<String>, response: Response<String>) {
            callback.onResponse(response.body().toString())
            }

        })
    }


    interface Apiservice {
        @GET("{path}")
        fun request(@Path("path") path: String):Call<String>
    }

}